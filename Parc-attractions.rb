class Attendee
    def initialize(height)
        @height = height
        @pass_id = nil
    end
    def height
        @height
    end
    def pass_id
        @pass_id
    end
    def issue_pass!(pass_id)
        @pass_id = pass_id
    end
    def revoke_pass!
        @pass_id = nil
    end
end


puts Attendee.new(106)
puts Attendee.new(106).height
puts Attendee.new(106).pass_id

attendee = Attendee.new(106)
attendee.issue_pass!(42)
puts attendee.pass_id

attendee = Attendee.new(106)
attendee.issue_pass!(42)
attendee.revoke_pass!
puts attendee.pass_id
